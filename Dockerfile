FROM adoptopenjdk/openjdk11:slim

ARG DEPENDENCY_CHECK_VERSION
ENV DEPENDENCY_CHECK_URL "http://dl.bintray.com/jeremy-long/owasp/dependency-check-$DEPENDENCY_CHECK_VERSION.zip"

RUN apt-get update && apt-get install -y unzip wget && rm -rf /var/lib/apt/lists/*
RUN wget $DEPENDENCY_CHECK_URL
RUN unzip dependency-check-$DEPENDENCY_CHECK_VERSION.zip
RUN ./dependency-check/bin/dependency-check.sh --updateonly