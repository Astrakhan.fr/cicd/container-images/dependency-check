# Dependency-check

## Purpose

Provide a container image including OWASP Dependency-check with its database already downloaded less than 24 hours ago.

## Usage

You can user this image using the following docker command line (`<directory-or-file-to-scan>`, `<output-report-dir>` and `<project-name>` have to replaced by your values, check the official [documentation](https://jeremylong.github.io/DependencyCheck/dependency-check-cli/arguments.html) if needed).

```shell
docker run \
  -v <directory-or-file-to-scan>:/scan-dir \
  -v <output-report-dir>:/report-dir \
  registry.gitlab.com/astrakhan.fr/cicd/container-images/dependency-check:5.2.0 \
  /dependency-check/bin/dependency-check.sh \
        --disableCentral --noupdate --cveValidForHours 24 \
        --scan /scan-dir \
        --format "ALL" \
        --project <project-name> \
        --failOnCVSS 7 \
        --out /report-dir
```

This image is made to get the GitLab CI job below able to check if there are any known, publicly disclosed, vulnerabilities.

```yml
.dependency-check:
  image: registry.gitlab.com/astrakhan.fr/cicd/container-images/dependency-check:5.2.0
  stage: test
  allow_failure: true
  variables:
    SECURITY_REPORTS_DIR: reports-security
    BINARIES_DIR: target
    GIT_STRATEGY: none
# Dependency check scan only Java (and .Net application if requirements are met)
# Uncomment the line below to enable experimental analyzers for Python, Ruby, PHP (composer), and Node.js applications
#    DEPENDENCY_CHECK_EXPERIMENTAL: --enableExperimental
  script:
    - mkdir -p $SECURITY_REPORTS_DIR
    - if ! /dependency-check/bin/dependency-check.sh $DEPENDENCY_CHECK_EXPERIMENTAL
        --disableNuspec --disableNugetconf --disableAssembly
        --disableCentral --noupdate --cveValidForHours 24
        --scan $BINARIES_DIR
        --format "ALL"
        --project $CI_PROJECT_NAME
        --failOnCVSS 7
        --out $SECURITY_REPORTS_DIR; then
          echo "---> There is vunerabilities with CVSS above 7. Please check the reports !" ;
          exit 1 ;
      fi
  artifacts:
    when: always
    paths:
      -  $SECURITY_REPORTS_DIR/
```
